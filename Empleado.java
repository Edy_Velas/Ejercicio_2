/**
 * Created by Baldemar on 08/07/2017.
 */
public class Empleado {
    private Integer NomEmpl;
    private Integer DpiEmpl;
    private Integer TelEmpl;

    public Empleado() {
    }

    public Empleado(Integer nomEmpl, Integer dpiEmpl, Integer telEmpl) {
        NomEmpl = nomEmpl;
        DpiEmpl = dpiEmpl;
        TelEmpl = telEmpl;
    }

    public Integer getNomEmpl() {
        return NomEmpl;
    }

    public void setNomEmpl(Integer nomEmpl) {
        NomEmpl = nomEmpl;
    }

    public Integer getDpiEmpl() {
        return DpiEmpl;
    }

    public void setDpiEmpl(Integer dpiEmpl) {
        DpiEmpl = dpiEmpl;
    }

    public Integer getTelEmpl() {
        return TelEmpl;
    }

    public void setTelEmpl(Integer telEmpl) {
        TelEmpl = telEmpl;
    }
}
