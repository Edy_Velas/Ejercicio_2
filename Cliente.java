/**
 * Created by Baldemar on 08/07/2017.
 */
public class Cliente {
    private String nomcliente;
    private String dircliente;
    private Integer telcliente;

    public Cliente() {
    }

    public Cliente(String nomcliente, String dircliente, Integer telcliente) {
        this.nomcliente = nomcliente;
        this.dircliente = dircliente;
        this.telcliente = telcliente;
    }

    public String getNomcliente() {
        return nomcliente;
    }

    public void setNomcliente(String nomcliente) {
        this.nomcliente = nomcliente;
    }

    public String getDircliente() {
        return dircliente;
    }

    public void setDircliente(String dircliente) {
        this.dircliente = dircliente;
    }

    public Integer getTelcliente() {
        return telcliente;
    }

    public void setTelcliente(Integer telcliente) {
        this.telcliente = telcliente;
    }
}
