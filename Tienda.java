import java.util.ArrayList;
import java.util.List;

/**
 * Created by Baldemar on 08/07/2017.
 */
public class Tienda {
    private String Nombre;
    private String Direccion;
    private Empleado Empleado;
    private Cliente Cliente;
    private Producto[] listado;
    private List<Producto> listadoProducto;
    private Integer Canti;

    public Tienda(int cantProducto) {
        listado = new Producto[cantProducto];
        Canti = 0;
        listadoProducto=new ArrayList<>();

    }

    public Empleado getEmpleado() {
        return Empleado;
    }

    public void setEmpleado(Empleado empleado) {
        Empleado = empleado;
    }

    public Cliente getCliente() {
        return Cliente;
    }

    public void setCliente(Cliente cliente) {
        Cliente = cliente;
    }

    public Producto[] getListado() {
        return listado;
    }

    public void setListado(Producto[] listado) {
        this.listado = listado;
    }

    public List<Producto> getListadoProducto() {
        return listadoProducto;
    }

    public void setListadoProducto(List<Producto> listadoProducto) {
        this.listadoProducto = listadoProducto;
    }

    public Integer getCanti() {
        return Canti;
    }

    public void setCanti(Integer canti) {
        Canti = canti;
    }



    public void adicionarprod(Producto d) {
        listadoProducto.add(d);
    }
}

