/**
 * Created by Baldemar on 08/07/2017.
 */
public class Producto {
    private String nomProducto;
    private Float valorunitProd;

    public Producto() {
    }

    public Producto(String nomProducto, Float valorunitProd) {
        this.nomProducto = nomProducto;
        this.valorunitProd = valorunitProd;
    }

    public String getNomProducto() {
        return nomProducto;
    }

    public void setNomProducto(String nomProducto) {
        this.nomProducto = nomProducto;
    }

    public Float getValorunitProd() {
        return valorunitProd;
    }

    public void setValorunitProd(Float valorunitProd) {
        this.valorunitProd = valorunitProd;
    }
}
