/**
 * Created by Baldemar on 08/07/2017.
 */

import java.util.Scanner;
import java.util.InputMismatchException;


public class Apptienda {
    public static void main (String[] args){
        Tienda tienda=null;
        Scanner sn = new Scanner(System.in);
        boolean salir = false;
        int opcion;

        while (!salir) {

            System.out.println("1. Ingresar cantidad de Productos");
            System.out.println("2. Agregar Producto");
            System.out.println("3. Listar Producto");
            System.out.println("4. Salir");

            System.out.println("Sistema Venta de Productos");

            try {
                System.out.println("Seleccione una de las opciones");
                opcion = sn.nextInt();

                switch (opcion) {
                    case 1:
                        System.out.println("Cuantos Productos Necesita Ingresar");
                        tienda = new Tienda(sn.nextInt());
                        break;
                    case 2:
                        Producto producto= new Producto();
                        System.out.println("Ingrese el nombre del Producto :");
                        producto.setNomProducto(sn.next());
                        System.out.println("Ingrese la edad del deportista: ");
                        producto.setValorunitProd(sn.nextFloat());
                        tienda.adicionarprod(producto);
                        break;
                    case 3:
                        for (Producto d:tienda.getListadoProducto())
                        {
                            System.out.printf(d.getNomProducto()+ "\n");

                        }
                        break;


                    case 4:
                        salir = true;
                        break;
                    default:
                        System.out.println("Solo números entre 1 y 5");
                }
            } catch (InputMismatchException e) {
                System.out.println("Debes insertar un número");
                sn.next();
            }
        }

    }

        }

